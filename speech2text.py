#!/usr/bin/env python
# license removed for brevity
import speech_recognition as sr
import rospy
from std_msgs.msg import String
#import speech_recognition as sr
def talker():
        pub = rospy.Publisher('chatter', String, queue_size=10)
        rospy.init_node('talker', anonymous=True)
        rate = rospy.Rate(10) # 10hz
        while not rospy.is_shutdown():
	    r = sr.Recognizer()
	    r.energy_threshold =48000
	    with sr.Microphone() as source:
    		r.adjust_for_ambient_noise(source)
    		print("Say something!")
    		audio = r.listen(source)#
    		print "Recognising........."# fron this onwards in after loop;
    		try:
    			print("Google Speech Recognition thinks you said " + r.recognize_google(audio))
        		textt=r.recognize_google(audio)
    		except sr.UnknownValueError:
    	 		print("Google Speech Recognition could not understand audio")
    		except sr.RequestError as e:
    	 		print("Could not request results from Google Speech Recognition service; {0}".format(e))
 
                rospy.loginfo(textt)
                pub.publish(textt)
                rate.sleep()
        

	
if __name__ == '__main__':
    try:
	talker()
    except rospy.ROSInterruptException:
        pass



