import speech_recognition as sr
import rospy
from std_msgs.msg import String
import speech_recognition as sr
def talker():
        pub = rospy.Publisher('chatter', String, queue_size=10)
        rospy.init_node('talker', anonymous=True)
        rate = rospy.Rate(10) # 10hz
        r = sr.Recognizer()
	r.energy_threshold =48000
	with sr.Microphone() as source:
    		r.adjust_for_ambient_noise(source)
    		print("Say something!")
    		audio = r.listen(source)#
                print "Recognising........."# 
                print("Google Speech Recognition thinks you said " + r.recognize_google(audio))
                textt=r.recognize_google(audio)


        while not rospy.is_shutdown():
                rospy.loginfo(textt)
                pub.publish(textt)
                rate.sleep()
        

if __name__ == '__main__':
    try:
	talker()
    except rospy.ROSInterruptException:
        pass


