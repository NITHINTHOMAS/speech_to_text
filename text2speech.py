#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from gtts import gTTS
import os 

def text(result):
    	tts = gTTS(text=result, lang='en')
    	tts.save("good.mp3")
    	os.system("mpg321 good.mp3")

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    a=data.data
    b=a.split()
    list1=['good','happy']
    list2=['bad','sad']
    count=0
    c=0
    for i in range (0,len(b)):
	for j in range (0,2):
        	if b[i]==list1[j]:
			count+=1
	for k in range (0,2):
		if b[i]==list2[k]:
			c+=1
    if count>c:
	result="you sounded positive"
        print "you sounded positive"
	text(result)
    elif count==c:
	result="you sounded neutral"
        print "you sounded neutral"
	text(result)
    else:
	result="you sounded negative"
        print "you sounded negative"
	text(result)
			
  
def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("chatter", String, callback)
    rospy.spin()


if __name__ == '__main__':
    listener()








